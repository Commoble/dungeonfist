package com.commoble.dungeonfist.common;

import com.commoble.dungeonfist.common.tileentity.TileEntityDungeonPortalBase;
import com.commoble.dungeonfist.common.tileentity.TileEntityLedger;
import com.commoble.dungeonfist.common.world.WorldGenManager;
import com.commoble.dungeonfist.common.world.WorldProviderDungeon;

import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraftforge.common.BiomeManager;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy
{
	
	// config values
	
	// dimensions
	public int test_dim_id;
	public DimensionType test_dim_type;
	
	// misc
	public static WorldGenManager worldGenManager = new WorldGenManager();
	
	public static int modEntityID = 0;
	
	/**
	 * Run before anything else;
	 * Declare configuration parameters;
	 * Register everything that doesn't require something else to be registered first
	 */
	public void preInit(FMLPreInitializationEvent event)
	{
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		Property conf;
		config.load();
		
		// TODO setup config file here
		
		config.save();
		
		// Registration
		// Blocks, Enchantments, Items, Potions, SoundEvents, and Biomes should be registered with registry events
		// Entities, Tile Entities, and Dimensions need to be registered here
		TileEntityLedger.registerTileEntities();
		//this.registerEntities();
		this.registerPlanes();
		GameRegistry.registerWorldGenerator(worldGenManager, 0);
	}
	
	/**
	 * The most important things to do in the main load event are:
	 * Register recipes, send FMLInterModComms messages to other mods, build data structures that shouldn't be in the other events
	 */
	public void load(FMLInitializationEvent event)
	{
		// register recipes
	}
	
	/**
	 * Handle interactions with other mods and complete setup
	 * e.g. registering creature spawning should go here due to other mods potentially creating new biomes
	 */
	public void postInit(FMLPostInitializationEvent event)
	{
		
	}
	
	// get free dim-IDs from the dimension manager and register planes
	protected void registerPlanes()
	{
		this.test_dim_id = DimensionManager.getNextFreeDimId();
		this.test_dim_type = DimensionType.register("the_dungeon", "_dungeon", this.test_dim_id, WorldProviderDungeon.class, false);
		DimensionManager.registerDimension(test_dim_id, test_dim_type);
	}
	
	////////////////////////////////////////////////////////
	
	// helper functions for particles
	// these do nothing and are overridden in the client proxy

	public void spawnDungeonPortalParticle(World world, double x, double y, double z, double vx, double vy, double vz, float red, float green, float blue)
	{
		return;
	}
}
