package com.commoble.dungeonfist.common.sound;

import com.commoble.dungeonfist.common.DungeonfistMod;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class SoundLedger
{
	
	public static void registerSounds()
	{
		
	}
	
	public static SoundEvent registerSound(String name)
	{
		ResourceLocation loc = new ResourceLocation(DungeonfistMod.MODID, name);
		return new SoundEvent(loc);
	}
}
