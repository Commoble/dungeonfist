package com.commoble.dungeonfist.common.block;

import com.commoble.dungeonfist.common.DungeonfistMod;

import net.minecraft.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Class for registering blocks and storing their references
 */
public class BlockLedger
{	
	public static BlockDungeonPortalBase dungeonPortalBase;
	public static BlockPortalGlowyAir portalGlowyAir;
	
	// register all the blocks, called by RegistryEventHandler
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		dungeonPortalBase = (BlockDungeonPortalBase)registerBlock(event.getRegistry(), new BlockDungeonPortalBase(), "dungeon_portal_base");
		portalGlowyAir = (BlockPortalGlowyAir)registerBlock(event.getRegistry(), new BlockPortalGlowyAir(), "portal_glowy_air");
	}
	
	private static Block registerBlock(IForgeRegistry<Block> registry, Block newBlock, String name)
	{
		name = DungeonfistMod.appendPrefix(name);
		newBlock.setUnlocalizedName(name);
		newBlock.setRegistryName(name);
		registry.register(newBlock);
		return newBlock;
	}
}
