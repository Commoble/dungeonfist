package com.commoble.dungeonfist.common.block;

import java.util.Random;

import javax.annotation.Nullable;

import com.commoble.dungeonfist.client.particle.ParticleDungeonPortal;
import com.commoble.dungeonfist.common.DungeonfistMod;
import com.commoble.dungeonfist.common.tileentity.TileEntityDungeonPortalBase;
import com.commoble.dungeonfist.common.world.TeleporterDungeon;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.network.play.server.SPacketEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * The primary portal blocks in the centers of portals
 */
public class BlockDungeonPortalBase extends BlockContainer
{
    public BlockDungeonPortalBase()
    {
        super(Material.ROCK);
        this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
        this.setSoundType(SoundType.GLASS);
        this.lightValue = 15;
        this.setTickRandomly(true);
        this.setBlockUnbreakable();
    }

    /**
     * Get the MapColor for this Block and the given BlockState
     */
    public MapColor getMapColor(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        return MapColor.RED;
    }

    /**
     * Whether this Block can be replaced directly by other blocks (true for e.g. tall grass)
     */
    @Override
    public boolean isReplaceable(IBlockAccess worldIn, BlockPos pos)
    {
        return false;
    }


    /**
     * Checks if this block can be placed exactly at the given position.
     */
    @Override
    public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
    {
    	// can place here if this and the two y-adjacent blocks are replaceable
    	// first, make sure it's not too close to the y-boundaries of the world
    	if (pos.getY() <= 6 || pos.getY() >= worldIn.getActualHeight() - 7)
    	{
    		return false;
    	}
    	for (int i=-1; i<=1; i++)
    	{
    		if (!worldIn.getBlockState(pos.up(i)).getBlock().isReplaceable(worldIn, pos.up(i)))
    		{
    			return false;
    		}
    	}
    	return super.canPlaceBlockAt(worldIn, pos);
    }

    /**
     * Called after the block is set in the Chunk data, but before the Tile Entity is set
     */
    @Override
    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
    {
    	super.onBlockAdded(worldIn, pos, state);
    	// set the two y-adjacent blocks to glowy air
		worldIn.setBlockState(pos.up(), BlockLedger.portalGlowyAir.getDefaultState());
		worldIn.setBlockState(pos.down(), BlockLedger.portalGlowyAir.getDefaultState());
    }

    /**
     * Called serverside after this block is replaced with another in Chunk, but before the Tile Entity is updated
     * when broken, break the two adjacent portal blocks
     */
    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        super.breakBlock(worldIn, pos, state);
    	if (worldIn.getBlockState(pos.up()).getBlock() == BlockLedger.portalGlowyAir)
    	{
    		worldIn.setBlockToAir(pos.up());
    	}
    	if (worldIn.getBlockState(pos.down()).getBlock() == BlockLedger.portalGlowyAir)
    	{
    		worldIn.setBlockToAir(pos.down());
    	}
    }

    /**
     * Called when the block is right clicked by a player.
     */
    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (world.isRemote || !(playerIn instanceof EntityPlayerMP))
        {
            return true;
        }
        else
        {
        	EntityPlayerMP mpPlayer = (EntityPlayerMP) playerIn;
	        if (!playerIn.isRiding() && !playerIn.isBeingRidden())
	        {
	        	// copied section from EntityPlayerMP.changeDimension
    			int targetID;	// id of dimension the player is teleporting to
    			if (world.provider.getDimensionType().getId() == 0)
    			{
    				targetID = DungeonfistMod.proxy.test_dim_id;
    			}
    			else
    			{
    				targetID = 0;
    			}
    			WorldServer serv = mpPlayer.mcServer.getWorld(targetID);	// will initialize world if it's not loaded
    			mpPlayer.getServer().getPlayerList().transferPlayerToDimension(mpPlayer, targetID, new TeleporterDungeon(serv, pos));


	            mpPlayer.connection.sendPacket(new SPacketEffect(1032, BlockPos.ORIGIN, 0, false));
	            mpPlayer.addExperience(0);	// set lastExperience to -1
	            mpPlayer.setPlayerHealthUpdated();	// set lastHealth to -1
	            //mpPlayer.lastFoodLevel = -1;	// no equivalent public function
                return true;
	        }
            return false;
        }
    }

    /**
     * The type of render function called. MODEL for mixed tesr and static model, MODELBLOCK_ANIMATED for TESR-only,
     * LIQUID for vanilla liquids, INVISIBLE to skip all rendering
     */
    public EnumBlockRenderType getRenderType(IBlockState state)
    {
        return EnumBlockRenderType.INVISIBLE;
    }

    @Nullable
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos)
    {
        return NULL_AABB;
    }

    /**
     * Used to determine ambient occlusion and culling when rebuilding chunks for render
     */
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    /**
     * Spawns this Block's drops into the World as EntityItems.
     */
    public void dropBlockAsItemWithChance(World worldIn, BlockPos pos, IBlockState state, float chance, int fortune)
    {
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    /**
     * Get the geometry of the queried face at the given position and state. This is used to decide whether things like
     * buttons are allowed to be placed on the face, or how glass panes connect to the face, among other things.
     * <p>
     * Common values are {@code SOLID}, which is the default, and {@code UNDEFINED}, which represents something that
     * does not fit the other descriptions and will generally cause other things not to connect to the face.
     * 
     * @return an approximation of the form of the given face
     */
    public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face)
    {
        return BlockFaceShape.UNDEFINED;
    }

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		// TODO Auto-generated method stub
		return new TileEntityDungeonPortalBase();
	}
}