package com.commoble.dungeonfist.common.item;

import com.commoble.dungeonfist.common.DungeonfistMod;
import com.commoble.dungeonfist.common.block.BlockLedger;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * Class for registering items and itemblocks, and keeping their references
 * also handle creative tabs since those are closely related to items
 */
public class ItemLedger
{
	// creative tab for the stuff
	public static final CreativeTabs dftab = new CreativeTabs("dungeonfist") {
		@Override
		public ItemStack getTabIconItem()
		{
			return new ItemStack(Items.GOLD_INGOT);
		}
	};
	
	public static ItemBlock dungeonPortalItemBlock;
	
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		dungeonPortalItemBlock = registerItemBlock(event.getRegistry(), new ItemBlock(BlockLedger.dungeonPortalBase), "dungeon_portal_base");
		dungeonPortalItemBlock.setCreativeTab(dftab);
	}
	
	private static Item registerItem(IForgeRegistry<Item> registry, Item newItem, String name)
	{
		name = DungeonfistMod.appendPrefix(name);
		newItem.setUnlocalizedName(name);
		newItem.setRegistryName(name);
		registry.register(newItem);
		return newItem;
	}
	
	private static ItemBlock registerItemBlock(IForgeRegistry<Item> registry, ItemBlock newItemBlock, String name)
	{
		name = DungeonfistMod.appendPrefix(name);
		newItemBlock.setUnlocalizedName(name);
		newItemBlock.setRegistryName(name);
		registry.register(newItemBlock);
		return newItemBlock;
	}
}
