package com.commoble.dungeonfist.common.world;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import java.util.Random;

import com.commoble.dungeonfist.common.block.BlockLedger;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockPortal;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockPattern;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldServer;

// TODO work the new location cache stuff into this
public class TeleporterDungeon extends Teleporter
{
	// the world the player is being teleported to
	protected BlockPos basePos;	// location of portal base in blockspace
    
    public TeleporterDungeon(WorldServer worldIn, BlockPos basePos)
    {
        super(worldIn);
        this.basePos = basePos;
    }

    @Override
    public void placeInPortal(Entity entityIn, float rotationYaw)
    {
        if (!this.placeInExistingPortal(entityIn, rotationYaw))
        {
            this.makePortal(entityIn);
            this.placeInExistingPortal(entityIn, rotationYaw);
        }
    }

    // dungeon portals use exact locations
    @Override
    public boolean placeInExistingPortal(Entity entityIn, float rotationYaw)
    {
        if (this.world.getBlockState(this.basePos).getBlock() == BlockLedger.dungeonPortalBase)
        {
            double spawnX = (double)this.basePos.getX() + 0.5D;
            double spawnZ = (double)this.basePos.getZ() + 0.5D;
            double spawnY = (double)this.basePos.getY(); 
            
            if (entityIn instanceof EntityPlayerMP)
            {
                ((EntityPlayerMP)entityIn).connection.setPlayerLocation(spawnX, spawnY, spawnZ, entityIn.rotationYaw, entityIn.rotationPitch);
            }
            else
            {
                entityIn.setLocationAndAngles(spawnX, spawnY, spawnZ, entityIn.rotationYaw, entityIn.rotationPitch);
            }

            return true;
        }
        else
        {
        	return false;
        }
    }

    // couldn't find an existing portal, so look for a reasonable place to create one and do that
    @Override
    public boolean makePortal(Entity entity)
    {
        //int i = 16;
        double candidateDistance = -1.0D;
        int playerX = MathHelper.floor(entity.posX);
        int playerY = MathHelper.floor(entity.posY);
        int playerZ = MathHelper.floor(entity.posZ);
        //int checkX = playerX;
        //int checkY = playerY;
        //int checkZ = playerZ;
        //int l1 = 0;
        //int i2 = this.random.nextInt(4);
        for (int xOff=-1; xOff<=1; xOff++)	//x offset
        {
        	for (int zOff=-1; zOff<=1; zOff++)	//y offset
        	{
        		if (xOff == 0 && zOff == 0)
        		{
        			// make the portal column, replace any block (the portal must flow)
        			// the flags cause a block update to update the lighting
        			world.setBlockState(this.basePos, BlockLedger.dungeonPortalBase.getDefaultState(), 3);
        			world.setBlockState(this.basePos.up(), BlockLedger.portalGlowyAir.getDefaultState(), 3);
        			world.setBlockState(this.basePos.down(), BlockLedger.portalGlowyAir.getDefaultState(), 3);
        		}
        		else
        		{
        			// remove blocks in this column
        			for (int yOff=-1; yOff<=1; yOff++)
        			{
        				BlockPos newPos = this.basePos.add(xOff, yOff, zOff);
        				// replace ANY block except tileentities
        				if (!(this.world.getBlockState(newPos) instanceof BlockContainer))
        				{
        					this.world.setBlockToAir(newPos);
        				}
        			}
        		}
        	}
        }

        return true;
    }
}