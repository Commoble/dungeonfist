package com.commoble.dungeonfist.common.world;

import net.minecraft.world.World;
import net.minecraft.world.gen.ChunkGeneratorHell;
import net.minecraft.world.gen.ChunkGeneratorOverworld;

public class ChunkGeneratorDungeon extends ChunkGeneratorHell
{

	public ChunkGeneratorDungeon(World worldIn, boolean mapFeaturesEnabledIn, long seed)
	{
		super(worldIn, mapFeaturesEnabledIn, seed);
	}

}
