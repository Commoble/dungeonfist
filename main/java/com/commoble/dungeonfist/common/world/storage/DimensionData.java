package com.commoble.dungeonfist.common.world.storage;

import com.commoble.dungeonfist.common.DungeonfistMod;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapStorage;
import net.minecraft.world.storage.WorldSavedData;

/**
 * Data storage class used to remember forge dimension ID -> dungeon hash mappings
 * this data is global (not bound to a specific World) and only used on the server worlds 
 */
public class DimensionData extends WorldSavedData
{
	private static final String DATA_NAME = DungeonfistMod.MODID + "_dimensiondata";
	
	public DimensionData()
	{
		super(DATA_NAME);
	}
	
	public DimensionData(String name)
	{
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public static DimensionData get(World world)
	{
		MapStorage storage = world.getMapStorage();	// this function signifies global data; use world.getPerWorldStorage() for per-world
		
		// attempt to get the DimensionData instance if it already exists
		DimensionData instance = (DimensionData)(storage.getOrLoadData(DimensionData.class, DATA_NAME));
		
		// otherwise, make a new one
		if (instance == null)
		{
			instance = new DimensionData();
			storage.setData(DATA_NAME, instance);
		}
		
		return instance;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
