package com.commoble.dungeonfist.common;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = DungeonfistMod.MODID, version = DungeonfistMod.VERSION, name=DungeonfistMod.NAME)
public class DungeonfistMod
{
	@Instance("ultimaterocks")	// the static instance of the mod class
	public static DungeonfistMod instance = new DungeonfistMod();
	
    public static final String MODID = "dungeonfist";
    public static final String VERSION = "1.0.0.0";
    public static final String NAME="Dungeonfist";
    
    @SidedProxy(clientSide="com.commoble.dungeonfist.client.CombinedClientProxy",
    		serverSide = "com.commoble.dungeonfist.server.DedicatedServerProxy")
    public static CommonProxy proxy;
    
    /**
     * Run before anything else; read the config, create blocks, items, etc, register w/ GameRegistry
     */
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	proxy.preInit(event);
    }
    
    /**
     * Setup anything that doesn't go in pre- or post-init. Build data structures, register recipes,
     * send FMLInterModComms messages to other mods
     */
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        proxy.load(event);
    }
    
    /**
     * Handle interaction with other mods, complete setup base on this
     */
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	proxy.postInit(event);
    }
    
    /**
     * Generates a string with the dungeonfist prefix from a base string to get the full string ID
     * e.g. "models/banana" -> "dungeonfist:models/banana"
     */
    public static String appendPrefix(String unprefixedString)
    {
		return DungeonfistMod.MODID + ":" + unprefixedString;
    }
}