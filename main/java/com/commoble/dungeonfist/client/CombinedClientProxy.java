package com.commoble.dungeonfist.client;

import com.commoble.dungeonfist.client.particle.ParticleDungeonPortal;
import com.commoble.dungeonfist.common.CommonProxy;
import com.commoble.dungeonfist.common.item.ItemLedger;
import com.commoble.dungeonfist.common.tileentity.TileEntityDungeonPortalBase;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
* CombinedClient is used to set up the mod and start it running when installed on a normal minecraft client.
* It should not contain any code necessary for proper operation on a DedicatedServer.
* Code required for both normal minecraft client and dedicated server should go into CommonProxy.
* 
* All client-side-specific things (rendering and textures, mostly) goes in here
*/
public class CombinedClientProxy extends CommonProxy
{
	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		super.preInit(event);	// run CommonProxy's preInit first to get things registered
		
		// entity renderers
		
		// tile entity renderers
	}
	
	@Override
	public void load(FMLInitializationEvent event)
	{
		super.load(event);
	}
	
	@Override
	public void postInit(FMLPostInitializationEvent event)
	{
		super.postInit(event);
	}
	
	// helper functions for spawning particles from code that runs on both client and server
	// these do nothing on the common proxy but are overridden here in the client
	
	// parameters are xyz position and velocity
	@Override
	public void spawnDungeonPortalParticle(World world, double x, double y, double z, double vx, double vy, double vz, float red, float green, float blue)
	{
		ParticleDungeonPortal part = new ParticleDungeonPortal(world, x, y, z, vx, vy, vz, red, green, blue);
		Minecraft.getMinecraft().effectRenderer.addEffect(part);
	}
}
