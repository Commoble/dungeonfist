package com.commoble.dungeonfist.client;

import com.commoble.dungeonfist.common.item.ItemLedger;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
public class ClientRegistryEventHandler
{
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		registerItemRenderer(ItemLedger.dungeonPortalItemBlock);
	}
	
	private static void registerItemRenderer(Item item)
	{
		ModelResourceLocation loc = new ModelResourceLocation(item.getRegistryName().toString(), "inventory");
		ModelLoader.setCustomModelResourceLocation(item, 0, loc);
	}
}
